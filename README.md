# nuxeo-addons

## bootstrap

```
docker exec -it <node container> bash

nuxeo bootstrap
```

## test

```
docker exec -it <java container> bash

mvn test
```
