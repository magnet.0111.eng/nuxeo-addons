package com.magnet.ns.services;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.inject.Inject;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.nuxeo.runtime.test.runner.Deploy;
import org.nuxeo.runtime.test.runner.PartialDeploy;
import org.nuxeo.runtime.test.runner.Features;
import org.nuxeo.runtime.test.runner.FeaturesRunner;
import org.nuxeo.runtime.test.runner.TargetExtensions;
import org.nuxeo.ecm.automation.test.AutomationFeature;
import org.nuxeo.ecm.core.test.DefaultRepositoryInit;
import org.nuxeo.ecm.core.test.annotations.Granularity;
import org.nuxeo.ecm.core.test.annotations.RepositoryConfig;

@RunWith(FeaturesRunner.class)
@Features({AutomationFeature.class})
@RepositoryConfig(init = DefaultRepositoryInit.class, cleanup = Granularity.METHOD)
@Deploy({
    "com.magnet.ns.example-core",
    "com.magnet.ns.example-core:OSGI-INF/test-component.xml"
})
public class TestComponent
{
    private static final Logger logger = LogManager.getLogger(TestComponent.class);

    @Inject
    protected Component component;

    @Test
    public void descriptorTest()
    {
        assertEquals(component.get("test.true").options.get("hoge.path"), "/path/to");
        assertEquals(component.get("test.false").options.get("hoge.key"), "Failer");
    }
}
