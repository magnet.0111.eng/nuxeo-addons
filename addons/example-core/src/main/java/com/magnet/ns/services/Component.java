package com.magnet.ns.services;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

import org.nuxeo.runtime.model.ComponentContext;
import org.nuxeo.runtime.model.ComponentInstance;
import org.nuxeo.runtime.model.DefaultComponent;

import com.magnet.ns.hoge.Descriptor;

public class Component extends DefaultComponent
{
    private static final Logger logger = LogManager.getLogger(Component.class);

    public static final String HOGE_XP = "hoge";

    protected final Map<String, Descriptor> configs = new HashMap<>();

    @Override
    public void registerContribution(Object contribution, String extensionPoint, ComponentInstance contributor)
    {
        logger.debug("registerContribution: " + extensionPoint);
        if (HOGE_XP.equals(extensionPoint)) {
            Descriptor descriptor = (Descriptor) contribution;
            configs.put(descriptor.name, descriptor);
        }

        super.registerContribution(contribution, extensionPoint, contributor);
    }

    @Override
    public void start(ComponentContext context)
    {
        logger.debug("Component has started.");
        super.start(context);
    }

    public Descriptor get(String name)
    {
        return configs.get(name);
    }
}
